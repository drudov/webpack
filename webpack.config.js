module.exports = {
  entry: './app/index.js',
  output: {
    path: './asset',
    filename: 'bundle.js'
  },
  module:{
    loaders: [
      {
        test: /\.js$/,
        loader: 'babel',
        exclude: /(node_modules|bower_components)/,
      }
    ]
  }
};